#!/usr/bin/python3
import os
import pathlib
import glob
from distutils.dir_util import copy_tree
from shutil import rmtree
from configuration import *

REQUESTS = 10000
REPEAT = 10

BITSTREAM = ' ' + BITSTREAM_B + ' '      #Add spaces so it can be used to call the main program

pris_ex = './prismatic_exerciser.py'

p = pathlib.Path(base_path + 'bitstreams_tests/')
#p = pathlib.Path(base_path + 'Done_projects/')

#for file in list(p.glob('prismatic_hbm_design_wrapper*bit')):
for file in list(p.glob('[0-9]*')):
	bitId = file.parts[-1].split('/',4)[-1]
	print('\n\n###################################################################')
	print('###                                                             ###')
	print('###\t\t' + bitId + '\t\t###')
	print('###                                                             ###')
	print('###################################################################\t\t')
	print(base_path + 'Done_projects/' + bitId)
	print(base_path + 'Prismatic_prj')
	copy_tree(base_path + 'Done_projects/' + bitId, base_path + 'Prismatic_prj')

	os.system( pris_ex + BITSTREAM + WARMUP + ' --requests ' + str(REQUESTS) + ' --repeat ' + str(REPEAT) + ' ' + bitId)

	WARMUP = ''     #make sure warmup happens only once at the beginning of the tests

