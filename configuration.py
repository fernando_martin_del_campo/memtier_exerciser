base_path = '/home/fmartin/projects/in_work/Prismatic_hbm-1.0/' #Path to the root of your Prismatic project
                                                                # (for example '/home/user/prj/Prismatic_hbm/')
                                                                # Make sure 'base_path/bitstreams_tests' exists

WARMUP = '--warmup'              #If you need to fill the memory use --warmup (will take forever).
                                 # Other way empty string

BITSTREAM_B = ' --bitstream '    #If you want to let this tool download the bitstreams, use --bitstream.
                                 # Other way, empty string.
                                 # For some reason, command line Vivado will not download some bitstreams
                                 # (who knows why, thanks Xilinx)

REQUESTS = 10000                 #Number of requests per thread per client
REPEAT = 10                      #Number of times each experiment is performed
