Prismatic exerciser
=====

A software application to excersice TCP-Prismatic through the use of memtier_benchmark 

Compatibility
---

This program requires python3 and memtier_benchmark to be installed.

To run:
---

A. Make sure run_prismatic.py and prismatic_exerciser.py have execution permissions for the user.

B. Make sure vivado can be executed.

C. Make sure the bitstreams_tests directory exist in the base directory of the Prismatic project.

D. (To be modified by something that makes more sense) In bitstreams_tests create an empty file or directory with the name of the each of the Prismatic combinations that will be tested, for example:

```
touch BASE_DIR/bitstreams_tests/2_2_1_1_1_1_2_2_2_2_2_1000_0_2_2_2_1
```

where BASE_DIR is the base directory of the Prismatic project.

E. Open configuration.py and modify its parameters accordingly.

F. Execute:

```
./run_prismatic.py
```

Results will appear in the data_out directory
