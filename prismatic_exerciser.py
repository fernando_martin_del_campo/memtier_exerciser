#!/usr/bin/python3 -x

import os
import re
from subprocess import run, TimeoutExpired, STDOUT, DEVNULL
import argparse
from itertools import repeat


def num(s):
	try:
		return float(s)
	except ValueError:
		return 0.0

##Define "tuples" to be used as arguments for the memtier benchmark
class argument:
	"""Class to define memtier_benchmark arguments"""
	def __init__(self, pre, val):
		self.pre = pre
		self.value = val

	def s(self):
		if self.pre == '':
			return ''
		elif self.value == None:
			return '--' + self.pre
		elif isinstance(self.value, int):
			return '--' + self.pre + '=' + str(self.value)
		else:
			return '--' + self.pre + '=' + self.value

## Function to perform a memtier request
def perform_requests(key_min_a, key_max_a, requests_a, ratio_s, instance, clients, threads, output_to_file = None, file_name = None):
	tool = 'memtier_benchmark'
	protocol = argument('protocol', 'memcache_binary')
	ip = argument('server', '10.1.2.120')
	port = argument('port', 7)
	threads = argument('threads', threads)
	clients = argument('clients', clients)
	key_min = argument('key-minimum', key_min_a)
	key_max = argument('key-maximum', key_max_a)
	rnd_data = argument('',None)
	#rnd_data = argument('random-data','')
	data_size = argument('data-size', 32)
	requests = argument('requests', requests_a)
	ratio = argument('ratio', ratio_s)

	if ( output_to_file == False ):
		file_out = DEVNULL
	else:
		file_out = open(file_name, mode='w+')

	retry = True
	retry_times = 3
	retry_current = 0

	while (retry):
		print('\t\tCurrent try: ' + str(retry_current))
		retry = False
		try:
			#run([tool, protocol.s(), ip.s(), port.s(), threads.s(), clients.s(), key_min.s(), key_max.s(), rnd_data.s(), data_size.s(), requests.s(), ratio.s()], stderr=STDOUT, timeout=5, stdout=file_out)
			run([tool, protocol.s(), ip.s(), port.s(), threads.s(), clients.s(), key_min.s(), key_max.s(), '--distinct-client-seed', '--random-data', '--randomize', '--hide-histogram', data_size.s(), requests.s(), ratio.s()], stderr=STDOUT, timeout=60, stdout=file_out)
		except TimeoutExpired:
			print("\t\t\tOops! Request timeout")
			retry_current = retry_current + 1
			if retry_current < retry_times:
				retry = True
				file_out.closed
				file_out = open(file_name, mode='w+')
			else:
				run(['vivado', '-mode', 'batch', '-source', '/home/fmartin/projects/in_work/Prismatic_hbm-1.0/write_bitstream.tcl', '-tclargs', instance])
	print("\n\t\t\tSuccess!\n")
	if ( output_to_file == True ):
		file_out.closed


#                                                                                                                                                                                                                      #
########################################################################################################################################################################################################################


## < Paths
BIT_PATH = '/home/fmartin/projects/in_work/Prismatic_hbm-1.0'
BIT_SCRIPT = 'write_bitstream.tcl'
BIT_WRITE = BIT_PATH + '/' +  BIT_SCRIPT
## Paths >

## < Arguments definitions
parser = argparse.ArgumentParser()
parser.add_argument("--verbosity", help="increase output verbosity")
parser.add_argument("-r","--requests", type=int, help="indicate the number of requests to perform")
parser.add_argument("-rep","--repeat", type=int, help="times a request (not warmup) is going to be repeated to generate statistics")
parser.add_argument("-w","--warmup", action="store_true", help="perform, one by one, write requests in the range of the requests")
parser.add_argument("-b","--bitstream", action="store_true", help="downloads the bitstream into the board before performing the requests")
parser.add_argument("instance", help="A string to identify the current instance of Prismatic")
args = parser.parse_args()
## Arguments definitions >


##An identifiying string needs to be given

## If no repeat argument was given, perform only once
if args.repeat != None:
	these_times = args.repeat
else:
	these_times = 1

data_dir = 'data_out/' + args.instance
try:
	os.mkdir(data_dir)
except OSError:
	print ("Creation of the directory %s failed. Aborting execution ..." % data_dir)
	exit(1)

## Download bitstream if asked to
if args.bitstream:
	run(['vivado', '-mode', 'batch', '-source', BIT_WRITE, '-tclargs', data_dir])

if args.verbosity:
    print("verbosity turned on")

## If asked for a warmup, perform a write request to every key in the defined range (1 to "requests")
if args.warmup:
	print('\nPREPARING WARMUP FOR ' + str(args.requests) + ' REQUESTS ...\n')
	for i in range( args.requests ):
		print('\tPerforming request #' + str(i+1))
		perform_requests( i+1, i+1, 1, '1:0', data_dir, 1, 1,  False, None )          #Plus 1 to start on 1. No output file
else:
	print('\nNo warmup requested\n')

print('PERFORMING WRITE TESTS ...\n')
print(these_times)
for j in range(these_times):
	print('\tPerforming full write test #' + str(j))
	perform_requests(1, 1000, args.requests, '1:0', data_dir, 10, 10, True, data_dir + "/write_reqs_" + args.instance + "_" + str(j) + ".out")

ops_sec_num = 0
hits_sec_num = 0
miss_sec_num = 0
latency_num = 0
data_sec_num = 0

file_totals = open(data_dir + "/write_totals_" + args.instance + ".csv", mode='w+')
for j in range(these_times):
	with open(data_dir + "/write_reqs_" + args.instance + '_' + str(j) + ".out", mode='r') as origin_file:
		for line in origin_file:
			#line = line.rstrip()
			if re.search(r'Sets', line):
				op, ops_sec, hits_sec, miss_sec, latency, data_sec = line.split( )
				ops_sec_num += num(ops_sec)
				hits_sec_num += num(hits_sec)
				miss_sec_num += num(miss_sec)
				latency_num += num(latency)
				data_sec_num += num(data_sec)

				file_totals.write( str(num(ops_sec)) + ',' +  str(num(hits_sec)) + ',' +  str(num(miss_sec)) + ',' +  str(num(latency)) + ',' +  str(num(data_sec)) + '\n')

file_totals.closed

file_avg = open(data_dir + "/write_average_" + args.instance + ".csv", mode='w+')
file_avg.write(str(ops_sec_num/args.repeat) + ',' + str(hits_sec_num/args.repeat) + ',' + str(miss_sec_num/args.repeat) + ',' + str(latency_num/args.repeat) + ',' + str(data_sec_num/args.repeat) + '\n')
file_avg.closed

print('PERFORMING READ TESTS ...\n')
for j in range(these_times):
	print('\tPerforming full read test #' + str(j))
	perform_requests(1, 1000, args.requests, '0:1', data_dir, 10, 10, True, data_dir + "/read_reqs_" + args.instance + "_" + str(j) + ".out")

ops_sec_num = 0
hits_sec_num = 0
miss_sec_num = 0
latency_num = 0
data_sec_num = 0

file_totals = open(data_dir + "/read_totals_" + args.instance + ".csv", mode='w+')
for j in range(these_times):
	with open(data_dir + '/read_reqs_' + args.instance + '_' + str(j) + '.out', mode='r') as origin_file:
		for line in origin_file:
			#line = line.rstrip()
			if re.search(r'Gets', line):
				op, ops_sec, hits_sec, miss_sec, latency, data_sec = line.split( )
				ops_sec_num += num(ops_sec)
				hits_sec_num += num(hits_sec)
				miss_sec_num += num(miss_sec)
				latency_num += num(latency)
				data_sec_num += num(data_sec)

				file_totals.write( str(num(ops_sec)) + ',' +  str(num(hits_sec)) + ',' +  str(num(miss_sec)) + ',' +  str(num(latency)) + ',' +  str(num(data_sec)) + '\n')

file_totals.closed

file_avg = open(data_dir + "/read_average_" + args.instance + ".csv", mode='w+')
file_avg.write(str(ops_sec_num/args.repeat) + ',' + str(hits_sec_num/args.repeat) + ',' + str(miss_sec_num/args.repeat) + ',' + str(latency_num/args.repeat) + ',' + str(data_sec_num/args.repeat) + '\n')
file_avg.closed
